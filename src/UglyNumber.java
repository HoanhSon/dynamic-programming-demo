import java.util.Scanner;

class UglyNumber {

	int getNthUglyNo(int index) {
		int ugly[] = new int[index];
		int i2 = 0, i3 = 0, i5 = 0;
		int numberMultipleOf2 = 2;
		int numberMultipleOf3 = 3;
		int numberMultipleOf5 = 5;
		int uglyNumberAtIndex = 1;

		ugly[0] = 1;

		for (int i = 1; i < index; i++) {

			uglyNumberAtIndex = Math.min(numberMultipleOf2, Math.min(numberMultipleOf3, numberMultipleOf5));
			ugly[i] = uglyNumberAtIndex;
			if (uglyNumberAtIndex == numberMultipleOf2) {
				i2++;
				numberMultipleOf2 = ugly[i2] * 2;
			}
			if (uglyNumberAtIndex == numberMultipleOf3) {
				i3++;
				numberMultipleOf3 = ugly[i3] * 3;
			}
			if (uglyNumberAtIndex == numberMultipleOf5) {
				i5++;
				numberMultipleOf5 = ugly[i5] * 5;
			}
		}

		return uglyNumberAtIndex;
	}

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);

		boolean validateDone = false;
		int number = 0;
		do {
			try {
				System.out.print("Enter a number: ");
				number = scanner.nextInt();
				validateDone = true;
			} catch (NumberFormatException e) {
				System.out.println("Invalid integer input");
			}
		} while (validateDone == false);

		UglyNumber uglyNumber = new UglyNumber();
		System.out.println(uglyNumber.getNthUglyNo(number));
	}
}